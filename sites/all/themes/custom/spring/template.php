<?php

	function spring_preprocess_page(&$variables){

		// if sidebar_left is occupied{
		// 	add cass of one-fourth to sidebar-left
		// 	add class three-quarters to content
		// }else{
		// 	add class full-width to content
		// 	add class of '' to sidebar-left
		// }

		$page = $variables['page'];
		$sidebar_class = 'one-fourths';
		$content_class = 'one-half';
		$sidebar_right = 'one-fourths';
		$variables['page']['tpl_control'] = [];


		if(empty($page['sidebar_left']) && empty($page['sidebar_right'])){
			$sidebar_class = '';
			$sidebar_right = '';
			$content_class = 'full-width';
		}
		else if(empty($page['sidebar_left'])){
			$sidebar_class = '';
			$sidebar_right = 'one-fourths';
			$content_class = 'three-fourths';
		}
		else if(empty($page['sidebar_right'])){
			$sidebar_class = 'one-fourths';
			$sidebar_right = '';
			$content_class = 'three-fourths';
		}
		else{
			$sidebar_class = 'one-quarter';
			$sidebar_right = 'one-quarter';
			$content_class = 'one-half';
		}

		// if(empty($page['sidebar_left'])){

		// 	$sidebar_class = '';
		// 	$content_class = 'full-width';

		// }

		$variables['page']['tpl_control']['sidebar_class'] = $sidebar_class;
		$variables['page']['tpl_control']['content_class'] = $content_class;
		$variables['page']['tpl_control']['sidebar_right'] = $sidebar_right;

	}